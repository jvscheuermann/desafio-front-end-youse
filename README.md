# Desafio Front-End Youse
Nesse repositiorio estão contidos todos os arquivos do desafio.
Na pasta UX estão os arquivos relacionados ao Layout.
  - Converti o arquivo em Sketch para o AdobeXD para poder trabalhar no meu notebook (Windows OS).

Na pasta DEVELOPMENT estão os arquivos relacionados ao desenvolvimento.
  - Utilizei Webpack 4 para o processo de build e desenvolvimento do site, as instruções para rodar estão no README dentro da pasta.
  - Utilizei JS ES6 (apesar de não existir quase codigo JS).
  - Utilizei SASS como pré processador.
  - Utilizei (tentei) a metodologia SRCSS

PS 01: Não deu tempo de aprender markdown.
PS 02: Trello do desafio: https://trello.com/b/4dTza9rw/youse
