const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

// Is the current build a development build
const DEVELOPMENT = (process.env.NODE_ENV === 'development');

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, 'www', 'app');
const dirAssets = path.join(__dirname, 'www', 'assets');

/**
 * Webpack Configuration
 */
module.exports = {
    entry: {
        bundle: path.join(dirApp, 'index.js')
    },
    resolve: {
        modules: [
            dirNode,
            dirApp,
            dirAssets
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: DEVELOPMENT ? '[name].css' : '[name].css',
            chunkFilename: DEVELOPMENT ? '[id].css' : '[id].css',
        }),

        // new webpack.DefinePlugin({
        //     DEVELOPMENT: DEVELOPMENT
        // }),

        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'www', 'index.html'),
            root: path.resolve(__dirname, 'www')
        })
    ],
    module: {
        rules: [
            // BABEL
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                options: {
                    compact: true
                }
            },

            // CSS / SASS
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    DEVELOPMENT ? 'style-loader' : MiniCssExtractPlugin.loader,
                    'css-loader',
                    'resolve-url-loader',
                    'postcss-loader',
                    'sass-loader'
                ]
            },

            // IMAGES
            {
                test: /\.(jpe?g|png|gif)$/,
                exclude: /fonts/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    outputPath: 'images/'
                }
            },

            // FONTS
            {
              test: /\.(woff2?|ttf|otf|eot|svg)$/,
              exclude: [/node_modules/, /images/],
              loader: 'file-loader',
              options: {
                  name: '[name].[ext]',
                  outputPath: 'fonts/'
              }
            },

            {
              test: /\.svg/,
              use: {
                //   loader: 'svg-url-loader',
                  loader: 'file-loader',
                  options: {
                    // encoding: 'base64'
                    name: '[name].[ext]',
                    outputPath: 'images/'
                  }
              }
          }
        ]
    }
};
