/**
 * Application entry point
 */

// Load application styles
import 'styles/index.scss';

// ================================
// START YOUR APP HERE
// ================================
function toggleClass (clickTarget, classTarget, className) {
  return clickTarget.addEventListener('click', function () {
    classTarget.classList.toggle(className)
  })
}

function swapNodes (targets) {
  for (let target of targets){
    let children = [ ...target.children ].reverse()
    children.reduce( (prev, curr) => {
      target.insertBefore(prev, curr)
      return curr
    }, children.shift())
  }
}


function createCollapser (target) {
  function handleCollapserClick () {
    target.classList.toggle('--collapsed')
  }
  
  target.addEventListener('click', handleCollapserClick)
}

function createCollapsers (targets) {
  for (let target of targets) createCollapser(target)
}

window.addEventListener('load', () => {
  const clickTarget = document.querySelector("#toggle")
  const navTarget =  document.querySelector("#nav-dropdown")

  toggleClass(clickTarget, clickTarget, '--close')
  toggleClass(clickTarget, navTarget, '--collapsed')


  const swapTargets = document.querySelectorAll("._swap")
  const collapserTargets = document.querySelectorAll(".duvida-collapser")
  
  
  if (window.innerWidth < 640) swapNodes(swapTargets)

  createCollapsers(collapserTargets)
})